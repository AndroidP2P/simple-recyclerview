package id.ihwan.recyclerview;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.ViewHolder> {

    private Context context;

    public ArrayList<Karyawan> getListKaryawan() {
        return listKaryawan;
    }

    public void setListKaryawan(ArrayList<Karyawan> listKaryawan) {
        this.listKaryawan = listKaryawan;
    }

    private ArrayList<Karyawan>listKaryawan;

    public RVAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemRow = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list, viewGroup, false);
        return new ViewHolder(itemRow);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        final String namaKlik = getListKaryawan().get(i).getNama();

        viewHolder.nama.setText("Nama : "+ getListKaryawan().get(i).getNama());
        viewHolder.jabatan.setText("Jabatan : "+getListKaryawan().get(i).getJabatan());
        viewHolder.asal.setText(getListKaryawan().get(i).getAsal());
        Glide.with(context).load(getListKaryawan().get(i).getGambar()).into(viewHolder.gambar);

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Kamu mengeklik "+namaKlik, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return getListKaryawan().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView nama, jabatan, asal;
        ImageView gambar;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            nama = itemView.findViewById(R.id.nama);
            jabatan = itemView.findViewById(R.id.jabatan);
            asal = itemView.findViewById(R.id.asal);
            gambar = itemView.findViewById(R.id.gambar);
            cardView = itemView.findViewById(R.id.cardview);
        }
    }
}
